//
// Created by argo on 17/08/17.
//

#ifndef SOLARSIM_LISTWIDGET_H
#define SOLARSIM_LISTWIDGET_H

#include <QMainWindow>
#include <QListWidget>
#include <QListWidgetItem>

class ListWidget : public QListWidget
{

public:
    explicit ListWidget(QWidget *parent = 0);

    QListWidgetItem * getQListWidgetItem(std::string st);

private:

protected slots:

};

#endif //SOLARSIM_LISTWIDGET_H
