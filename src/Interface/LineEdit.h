//
// Created by argo on 24/08/17.
//

#ifndef SOLARSIM_LINEEDIT_H
#define SOLARSIM_LINEEDIT_H

#include <QtWidgets/QWidget>
#include <QLineEdit>

class LineEdit : public QLineEdit
{

public:
    explicit LineEdit(QWidget *parent = 0);

    QString textFloat;

private:

protected slots:
    void handleChangeText(const QString &text);
};

#endif //SOLARSIM_LINEEDIT_H
