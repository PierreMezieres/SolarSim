//
// Created by argo on 15/08/17.
//
#include <QtWidgets/QListWidgetItem>
#include "ComboBox.h"

ComboBox::ComboBox(QWidget *parent) :
        QComboBox(parent)
{


}

int ComboBox::getIndex(std::string st){
    QSize size=this->size();
    int s=size.height();
    for(int i=0;i<=s;i++){
        if(this->itemText(i).toStdString()==st) return i;
    }
    return -1;
}

void ComboBox::add(std::string st){
    QString ch = QString::fromStdString(st);
    this->addItem(ch);
}

void ComboBox::remove(std::string st){
    this->removeItem(getIndex(st));
}

void ComboBox::removeAll(){
    this->clear();
}


Unit::Unit_Term ComboBox::getUnit(std::string st){
    if(st=="UA") return Unit::UA;
    if(st=="Km") return Unit::Km;
    if(st=="TKm" || st=="Thousand Km") return Unit::TKm;
    if(st=="MKm" || st=="Million Km") return Unit::MKm;
    if(st=="BKm" || st=="Billion Km") return Unit::BKm;
}



Unit::Unit_Term ComboBox::getUnitIndex(){
    switch(this->currentIndex()){
        case 0: return Unit::UA;
        case 1: return Unit::Km;
        case 2: return Unit::TKm;
        case 3: return Unit::MKm;
        case 4: return Unit::BKm;
        default: break;
    }
    return Unit::UA;
}

Unit::Unit_Time ComboBox::getUnitIndex2(){
    switch(this->currentIndex()){
        case 0: return Unit::SEC;
        case 1: return Unit::MIN;
        case 2: return Unit::HOU;
        case 3: return Unit::DAY;
        case 4: return Unit::MON;
        case 5: return Unit::YEA;
        default: break;
    }
    return Unit::SEC;
}


Unit::Unit_Term ComboBox::getUnitIndex(int i){
    switch(i){
        case 0: return Unit::UA;
        case 1: return Unit::Km;
        case 2: return Unit::TKm;
        case 3: return Unit::MKm;
        case 4: return Unit::BKm;
        default: break;
    }
    return Unit::UA;
}

Unit::Unit_Time ComboBox::getUnitIndex2(int i){
    switch(i){
        case 0: return Unit::SEC;
        case 1: return Unit::MIN;
        case 2: return Unit::HOU;
        case 3: return Unit::DAY;
        case 4: return Unit::MON;
        case 5: return Unit::YEA;
        default: break;
    }
    return Unit::SEC;
}