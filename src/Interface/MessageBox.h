//
// Created by argo on 16/08/17.
//

#ifndef SOLARSIM_MESSAGEBOX_H
#define SOLARSIM_MESSAGEBOX_H




#include <QtWidgets/QMessageBox>
#include <string>
using namespace std;

class MessageBox{
public:

    enum Icon{NO_ICON, WARNING_ICON, INFO_ICON};
    enum Color{NO_COLOR, RED, GREEN};

    explicit MessageBox(string title, string message, Color color=NO_COLOR, Icon icon=NO_ICON, bool exec=true);

    bool exec();
    void addButton(QMessageBox::StandardButton standardButton);
    void setIcon(Icon icon);
    void setColor(string message,Color color);

    QMessageBox messageBox;

};

#endif //SOLARSIM_MESSAGEBOX_H
