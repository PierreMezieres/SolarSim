//
// Created by argo on 16/08/17.
//

#include "String.h"

string String::replacePiece(string s,string one,string two){
    string res=s;
    int place;
    while((place=res.find(one))!=-1){
        res=res.substr(0,place) + two + res.substr(place+one.size());
    }
    return res;
}

string String::colorize(string s,Color color){
    string col="";
    switch(color){
        case RED: col="F7230C"; break;
        case GREEN: col="22780F"; break;
        default: break;
    }
    string s2=replacePiece(s,"\n","<br>");
    return "<span style = color:'#"+col+"'>"+s2+"</span>";
}