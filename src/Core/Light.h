//
// Created by argo on 24/08/17.
//

#ifndef SOLARSIM_LIGHT_H
#define SOLARSIM_LIGHT_H

#include <GL/gl.h>

class Light{

public:

    static int numberLight();
    static GLenum getLight(int i);
    static int getNumber(GLenum i);
};

#endif //SOLARSIM_LIGHT_H
