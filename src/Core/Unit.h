//
// Created by argo on 21/08/17.
//

/******************************************************
 * Static Class to manage Unit of the System
 *
 * NB:
 * The unit is used in Tkm and not in Km
 ******************************************************/

#ifndef SOLARSIM_UNIT_H
#define SOLARSIM_UNIT_H

#define UNIT_BASE 150000

class Unit{
public:
    enum Unit_Term{UA,Km,TKm,MKm,BKm}; //Term possible for unit
    enum Unit_Time{SEC,MIN,HOU,DAY,MON,YEA};

    struct Distance{
        float val;
        Unit_Term dist;
    };

    struct Time{
        float val;
        Unit_Time time;
    };

    static Distance size;
    static Distance high;
    static Time speed;
    static Time speedA;

    //static Unit_Term size; static int sizeI; //Size unit
    //static Unit_Term high; static int highI; //High unit
    //static Unit_Term speed; static int speedI; static Unit_Time speed2; //Speed unit


    static float getKmFromUnit(Unit_Term term);
    static float getUAFromUnit(Unit_Term term);
    static float getValUnit(Distance distance); //Return the value in Km
    static float getValUnit2(Time time);
    static float getSizeUnit();
    static float getHighUnit();
    static float getSpeedUnit();
    static float getSpeedAUnit();
};

#endif //SOLARSIM_UNIT_H
