//
// Created by argo on 15/06/17.
//

/******************************************************
 * Static Class to manage easily vec3
 ******************************************************/

#ifndef SOLARSIM_VECTOR3_H
#define SOLARSIM_VECTOR3_H

#include <glm/vec3.hpp>

class Vector3 {
public:
    static float length(glm::vec3 vector);
    static float dotProduct(glm::vec3 vec1, glm::vec3 vec2);
    static glm::vec3 crossProductFloat(glm::vec3 vec1, glm::vec3 vec2);
};

#endif //SOLARSIM_VECTOR3_H
