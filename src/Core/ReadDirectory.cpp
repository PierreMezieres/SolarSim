//
// Created by argo on 14/08/17.
//

#include <src/Debug/Log.h>
#include "ReadDirectory.h"


DIR * ReadDirectory::rep= nullptr;
struct dirent * ReadDirectory::ent= NULL;


int ReadDirectory::isFile(string * s){
    if(*s=="." || *s=="..") return 0;
    return 1;
}

string ReadDirectory::nameWithoutExtension(string s){
    int pos=s.find('.');
    if(pos>=s.length()) return s;
    return s.substr(0,pos);
}

void ReadDirectory::iteratorFirst(string id){
    const char * s=id.data();
    rep=opendir(s);
    if(rep==NULL){
        ent= nullptr;
    }
}

int ReadDirectory::iteratorHasNext(){
    if(ent==nullptr){
        return 0;
    }
    return 1;
}

struct dirent * ReadDirectory::iteratorNext(){
    ent=readdir(rep);
    return ent;
}

string ReadDirectory::iteratorNextS(){
    if(rep==NULL) return string("PROBLEME1");
    ent=readdir(rep);
    if(ent==NULL) return string("PROBLEME2");
    string ret=ent->d_name;
    return ret;
}

bool ReadDirectory::iteratorNextS(string *s){
    if(rep==NULL) return false;
    ent=readdir(rep);
    if(ent==NULL) return false;
    (*s)=ent->d_name;
    return true;
}

bool ReadDirectory::fileExist(string dir,string file){
    ReadDirectory::iteratorFirst(dir);
    string f=ReadDirectory::iteratorNextS();
    do{
        if(f==file) return true;
        f=ReadDirectory::iteratorNextS();
    }while(ReadDirectory::iteratorHasNext());
    return false;
}