//
// Created by argo on 11/08/17.
//

/******************************************************
 * Class to do generic list
 *
 * NB:
 * Iterator available
 ******************************************************/

#ifndef SOLARSIM_LIST_H
#define SOLARSIM_LIST_H

#include <iostream>
#include <vector>
#include "../Debug/Log.h"
#include <cstdlib>
using namespace std;

template <class T>
class List {
public:

    //Attribute
    vector<T> list;
    int it=0; //iterator

    void add(T object); //add an object
    void eraseAll(); //Erase all the object
    void erase(T object); //Erase the object
    int isEmpty(); //Return 1 if it's empty, 0 else
    bool contain(T object);

    /****** Iterator ******/
    void iteratorFirst(); //Place the itr on first position
    int iteratorHasNext(); //Return 1 if itr has next, 0 else
    T iteratorNext(); //Return current object and itr going to next


private:

};


#endif //SOLARSIM_LIST_H

template <class T>
void List<T>::add(T object){
    list.push_back(object);
    iteratorFirst();
}

template <class T>
void List<T>::eraseAll(){
    list.clear();
    it=0;
}

template <class T>
void List<T>::erase(T object){
    iteratorFirst();
    int i=0;
    while(iteratorHasNext()){
        T n=iteratorNext();
        if(n==object){
            list.erase(list.begin()+i);
            it=0;
        }
        i++;
    }
}

template <class T>
int List<T>::isEmpty(){
    if(list.size()==0) return 1;
    return 0;
}

template <class T>
bool List<T>::contain(T object){
    iteratorFirst();
    while(iteratorHasNext()){
        T n=iteratorNext();
        if(n==object) return true;
    }
    return false;
}

template <class T>
void List<T>::iteratorFirst(){
    it=0;
}

template <class T>
int List<T>::iteratorHasNext(){
    if(it>=list.size()) return 0;
    return 1;
}

template <class T>
T List<T>::iteratorNext(){
    it++;
    return list[it-1];
}




