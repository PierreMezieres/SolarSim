//
// Created by argo on 30/08/17.
//
#include <src/Debug/Log.h>
#include "ReadFile.h"

ifstream * ReadFile::file= nullptr;
ofstream * ReadFile::fileW = nullptr;

void ReadFile::openFile(string s){
    fileW=new ofstream(s, ios::out | ios::trunc);
}

void ReadFile::writeLine(string s){
    if(fileW!=nullptr){
        (*fileW) << s <<endl;
    }
}

void ReadFile::iteratorFirst(string id){
    file=new ifstream(id);
}

int ReadFile::iteratorHasNextLine(){
    if(file=NULL) return 0;
    if(file->eof()) return 0;
    return 1;
}

string ReadFile::iteratorNextLine(){
    string line;
    getline((*file),line);
    return line;
}

string ReadFile::iteratorNextString(){
    string res;
    *file >> res;
    return res;
}

float ReadFile::iteratorNextFloat(){
    float res;
    *file >> res;
    return res;
}

int ReadFile::iteratorNextInt(){
    int res;
    *file >> res;
    return res;
}