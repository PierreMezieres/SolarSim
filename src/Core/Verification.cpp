//
// Created by argo on 16/08/17.
//

#include <src/Debug/Log.h>
#include <cmath>
#include "Verification.h"

bool caracterAutorized(char c){
    if(c>='0' && c<='9') return true;
    if(c=='.') return true;
    return false;
}

bool Verification::verifFloat(string s,float *res){
    int size=s.size();
    float r=0.0;
    bool pointSeen=false;
    int decal=0;
    for(int i=0;i<size;i++){
        if(caracterAutorized(s[i])){
            if(s[i]=='.'){
                if(pointSeen) return false;
                pointSeen=true;
            }else{
                int nouv=s[i]-'0';
                if(!pointSeen){
                    r=r*10+nouv;
                }else{
                    decal++;
                    r=r+nouv/pow(10,decal);
                }
            }
        }else return false;
    }
    if(res!= nullptr) *res=r;
    return true;
}