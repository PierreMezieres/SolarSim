//
// Created by argo on 16/08/17.
//

/******************************************************
 * Static Class to do some verification
 * (on all kind of parameter)
 ******************************************************/

#ifndef SOLARSIM_VERIFICATION_H
#define SOLARSIM_VERIFICATION_H

#include <string>
using namespace std;

class Verification{
public:
    static bool verifFloat(string s,float *res = nullptr);
};

#endif //SOLARSIM_VERIFICATION_H
