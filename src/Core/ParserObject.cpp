//
// Created by argo on 30/08/17.
//
#include <src/Engine/Renderer/Element/Planet.h>
#include <src/Engine/Renderer/Element/Sun.h>
#include <src/Debug/Log.h>
#include "ParserObject.h"
#include "ReadFile.h"

bool ParserObject::next=true;
ManagerTexture * ParserObject::managerTexture=nullptr;
ManagerRenderer * ParserObject::managerRenderer= nullptr;

void ParserObject::openFile(string id){
    ReadFile::iteratorFirst("../save/"+id);
    next=true;
}

void ParserObject::addManagerTexture(ManagerTexture *managerTexture){
    ParserObject::managerTexture=managerTexture;
}

void ParserObject::addManagerRenderer(ManagerRenderer *managerRenderer){
    ParserObject::managerRenderer=managerRenderer;
}

void ParserObject::getNextPlanet(){
    string type=ReadFile::iteratorNextString();
    string id=ReadFile::iteratorNextString();
    string texture=ReadFile::iteratorNextString();
    string size=ReadFile::iteratorNextString();
    int unitSize=ReadFile::iteratorNextInt();
    string high=ReadFile::iteratorNextString();
    int unitHigh=ReadFile::iteratorNextInt();
    string period=ReadFile::iteratorNextString();
    int unitPeriod=ReadFile::iteratorNextInt();
    string tilt=ReadFile::iteratorNextString();
    string day=ReadFile::iteratorNextString();
    int unitDay=ReadFile::iteratorNextInt();
    int num_light;
    if(type=="Sun") num_light=ReadFile::iteratorNextInt();
    if(type!="EOF") {
        Log::debug("------- New Object -------");
        Log::debug("Type : " + type);
        Log::debug("Name : " + id);
        Log::debug("Texture : " + texture);
        Log::intValue("size : " + size, unitSize);
        Log::intValue("high : " + high, unitHigh);
        Log::intValue("period : " + period, unitPeriod);
        Log::intValue("tilt : " + tilt + ".Day : " + day, unitDay);
        if(managerTexture!= nullptr){
            if(type=="Planet"){
                Texture * texture2=managerTexture->getObjectTextureFromId(texture);
                Planet *planet=new Planet(id,texture2,size,unitSize,high,unitHigh,period,unitPeriod,tilt,day,unitDay);
                managerRenderer->add(planet,true,true);
            }
            if(type=="Sun"){
                Texture * texture2=managerTexture->getObjectTextureFromId(texture);
                Sun *sun=new Sun(num_light,id,texture2,size,unitSize,high,unitHigh,period,unitPeriod,tilt,day,unitDay);
                managerRenderer->add(sun,true,true);
            }
        }else{
            Log::warning("ParserObject: no Manager Texture given !");
        }
    }else next=false;
}

int ParserObject::hasNextPlanet(){
    if(next) return 1;
    return 0;
}

void ParserObject::addFileToRender(string id){
    openFile(id);
    if(managerTexture==nullptr) Log::warning("Manager Texture not added, impossible to load file");
    if(managerRenderer==nullptr) Log::warning("Manager Renderer not added, impossible to load file");
    while(hasNextPlanet()){
        getNextPlanet();
    }
}