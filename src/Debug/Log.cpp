//
// Created by argo on 06/06/17.
//
#include <iostream>
#include "Log.h"

void Log::info(std::string m){
    std::cout << "INFO : " << m << "\n";
}

void Log::print(std::string m){
    std::cout << "  " << m;
}

void Log::debug(std::string m){
    std::cout << "\033[34mDEBUG : " << m << "\033[0m \n";
}

void Log::warning(std::string m){
    std::cout << "\033[31m /!\\ WARNING /!\\ " << m << "\033[0m \n";
}

void Log::success(std::string m){
    std::cout << "\033[32mSUCCESS : " << m << "\033[0m \n";
}

void Log::intValue(std::string m, int val){
    std::cout << "\033[34mVALUE : " << m << " : " << val << "\033[0m \n";
}

void Log::floatValue(std::string m, float val){
    std::cout << "\033[34mVALUE : " << m << " : " << val << "\033[0m \n";
}