//
// Created by argo on 06/06/17.
//

/******************************************************
 * Static class to write some log on terminal
 ******************************************************/

#ifndef SOLARSIM_LOG_H
#define SOLARSIM_LOG_H

#include <string>
#include <typeinfo>

class Log
{

public:

    //Print in white -> INFO : message
    static void info(std::string m);

    //Print in white -> message
    static void print(std::string m);

    //Print in blue -> DEBUG : message
    static void debug(std::string m);

    //Print in red ->  /!\ WARNING /!\ : message
    static void warning(std::string m);

    //Print in green -> SUCCESS : message
    static void success(std::string m);

    //Print in blue -> VALUE : message : val
    static void intValue(std::string m, int val);

    //Print in blue -> VALUE : message : val
    static void floatValue(std::string m, float val);

};

#endif //SOLARSIM_LOG_H
