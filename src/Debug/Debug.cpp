//
// Created by argo on 13/06/17.
//

#include <src/Core/String.h>
#include <cmath>
#include <src/Core/ReadFile.h>
#include <src/Core/ParserObject.h>
#include "../Core/ReadDirectory.h"
#include "Debug.h"
#include "../Engine/Renderer/ListRenderer.h"
#include "Log.h"

void Debug::verificationStart(){
    Log::print("\n\n");
    Log::debug(" <<<<< START SEQUENCE >>>>>");
    int ret=0;

    testReadDirectory();
    testReadFile();

    if(!ret) ret = testListRenderer();
    if(!ret) ret = testList();
    if(!ret) ret = testString();

    if(ret){
        Log::warning("START SEQUENCE FAIL");
        Log::warning("The Start Sequence is a failure. Do not hesitate to contact devs !");
        Log::warning("If you can, join a screenshot of the start sequence, thank you :)\n");
    }else{
        Log::success(" <<<<< START SEQUENCE SUCCESS >>>>>");
        Log::info("Even if the \"Start Sequence is a success\" multiple function");
        Log::info("are not tested. If you find a bug, do not hesitate to contact devs ! :)");
        Log::info("Enjoy SolarSim !\n\n");
    }
}

int Debug::testListRenderer(){
    Log::debug(" /// List ///");
    ListRenderer listRenderer;
    listRenderer.add();
    listRenderer.add();
    listRenderer.add();
    Log::debug("Added three object");
    if(listRenderer.iteratorHasNext()){
        Log::debug("First Object OK");
    }else{
        Log::warning("First Object KO"); return 1;
    }
    listRenderer.iteratorNext();
    if(listRenderer.iteratorHasNext()){
        Log::debug("Second Object OK");
    }else{
        Log::warning("Second Object KO"); return 1;
    }
    listRenderer.iteratorNext();
    if(listRenderer.iteratorHasNext()){
        Log::debug("Third Object OK");
    }else{
        Log::warning("Third Object KO"); return 1;
    }
    listRenderer.iteratorNext();
    if(listRenderer.iteratorHasNext()){
        Log::warning("Iterator KO"); return 1;
    }else{
        Log::debug("Iterator OK");
    }
    Log::debug("Iterator reset");
    listRenderer.iteratorFirst();
    listRenderer.iteratorNext();
    listRenderer.iteratorNext();
    if(listRenderer.iteratorHasNext()){
        Log::debug("First/Second Object OK");
    }else{
        Log::warning("First/Second Object KO"); return 1;
    }
    listRenderer.iteratorNext();
    if(listRenderer.iteratorHasNext()){
        Log::warning("Iterator KO"); return 1;
    }else{
        Log::debug("Iterator OK");
    }

    listRenderer.add();
    Log::debug("Added another object");
    if(listRenderer.iteratorHasNext()){
        Log::debug("Test OK");
    }else{
        Log::warning("Test KO"); return 1;
    }
    listRenderer.eraseAll();
    Log::debug("Erase All list");
    if(listRenderer.iteratorHasNext()){
        Log::warning("Test KO"); return 1;
    }else{
        Log::debug("Test OK");
    }
    Log::print("\n");
    return 0;
}

int Debug::testList(){
    Log::debug(" /// ListG ///");
    List<int> list;
    int i=11;
    list.add(i);
    i=222;
    list.add(i);
    i=3;
    list.add(i);

    /*vector<int>::iterator it;
    for(it=list.list.begin(); it < list.list.end(); it++){
        cout << *it << "\n";
    }*/

    Log::debug("Added three object");
    if(list.iteratorHasNext()){
        Log::debug("First Object OK");
    }else{
        Log::warning("First Object KO"); return 1;
    }
    int test=list.iteratorNext();
    if(list.iteratorHasNext()){
        Log::debug("Second Object OK");
    }else{
        Log::warning("Second Object KO"); return 1;
    }
    test=list.iteratorNext();
    if(list.iteratorHasNext()){
        Log::debug("Third Object OK");
    }else{
        Log::warning("Third Object KO"); return 1;
    }
    test=list.iteratorNext();
    if(list.iteratorHasNext()){
        Log::warning("Iterator KO"); return 1;
    }else{
        Log::debug("Iterator OK");
    }
    Log::debug("Iterator reset");
    list.iteratorFirst();
    list.iteratorNext();
    list.iteratorNext();
    if(list.iteratorHasNext()){
        Log::debug("First/Second Object OK");
    }else{
        Log::warning("First/Second Object KO"); return 1;
    }
    list.iteratorNext();
    if(list.iteratorHasNext()){
        Log::warning("Iterator KO"); return 1;
    }else{
        Log::debug("Iterator OK");
    }
    list.add(i);
    Log::debug("Added another object");
    if(list.iteratorHasNext()){
        Log::debug("Test OK");
    }else{
        Log::warning("Test KO"); return 1;
    }
    list.eraseAll();
    Log::debug("Erase All list");
    if(list.iteratorHasNext()){
        Log::warning("Test KO"); return 1;
    }else{
        Log::debug("Test OK");
    }
    list.add(i);
    Log::debug("Added another object");
    if(list.iteratorHasNext()){
        Log::debug("Test OK");
    }else{
        Log::warning("Test KO"); return 1;
    }
    Log::print("\n");
    return 0;
}

void Debug::testReadDirectory(){
    ReadDirectory::iteratorFirst("../textures");
    Log::print("\n\n");
    Log::debug("Contenu dossier textures");
    string file=ReadDirectory::iteratorNextS();
    do{
        if(ReadDirectory::isFile(&file)) {
            Log::debug("->" + ReadDirectory::nameWithoutExtension(file));
        }
        file=ReadDirectory::iteratorNextS();
    }while(ReadDirectory::iteratorHasNext());
    Log::debug("Fin contenu dossier\n\n");
}

void Debug::testReadFile(){
}

int Debug::testString(){
    string test="Petit test pour les String";
    Log::debug("expected:Des test pour les String -> "+String::replacePiece(test,"Petit","Des"));
    if(String::replacePiece(test,"Petit","Des")!="Des test pour les String"){
        Log::warning("Test KO"); return 1;
    }else{
        Log::debug("Test OK");
    }
    Log::debug("expected:Petit test pour les blob -> "+String::replacePiece(test,"String","blob"));
    if(String::replacePiece(test,"String","blob")!="Petit test pour les blob"){
        Log::warning("Test KO"); return 1;
    }else{
        Log::debug("Test OK");
    }
    return 0;
}

