//
// Created by argo on 07/06/17.
//

#ifndef SOLARSIM_GLWIDGET_H
#define SOLARSIM_GLWIDGET_H


#include <src/Engine/Camera.h>
#include <QtWidgets/QOpenGLWidget>
#include <QTimer>
#include <QKeyEvent>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class GLWidget : public QOpenGLWidget {
Q_OBJECT
public:
    GLWidget( int framesPerSecond = 0, QWidget *parent = 0, char *name = 0);

    virtual void initializeGL() = 0;

    virtual void resizeGL(int width, int height) = 0;

    virtual void paintGL() = 0;

    virtual void keyPressEvent(QKeyEvent *keyEvent);

    virtual void mouseMoveEvent(QMouseEvent *mouse);

    virtual void mouseReleaseEvent(QMouseEvent *event);

    virtual void mousePressEvent(QMouseEvent *event);

    virtual void wheelEvent(QWheelEvent *event);

    Camera camera=Camera(glm::vec3(0.0f, 0.0f, 3.0f));
    GLint lastX,lastY;

public slots:

    virtual void timeOutSlot();

private:
    QTimer *t_Timer;

};

#endif //SOLARSIM_GLWIDGET_H
