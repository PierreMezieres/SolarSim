//
// Created by argo on 30/08/17.
//

#ifndef SOLARSIM_DIALOGLOADSAVE_H
#define SOLARSIM_DIALOGLOADSAVE_H

#include <QtWidgets/QDialog>
#include <src/Engine/Renderer/DefaultSystem.h>
#include <src/Gui/MainWindow.h>

namespace Ui {
    class DialogLoadSave;
}

class DialogLoadSave : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLoadSave(MainWindow *mainWindow,QWidget *parent = 0);
    ~DialogLoadSave();


private:
    Ui::DialogLoadSave*ui;
    MainWindow *mainWindow;
    DefaultSystem *defaultSystem;

    bool defaultChoice; QListWidgetItem *choice;

    void fullfillDefault();
    void fullfillUser();
    bool verifSave();

protected slots:

    void handleCharge();
    void handleSave();
    void handleRemove();
    void handleActivatedItem(QListWidgetItem * item);
    void handleActivatedItem_2(QListWidgetItem * item);

};

#endif //SOLARSIM_DIALOGLOADSAVE_H
