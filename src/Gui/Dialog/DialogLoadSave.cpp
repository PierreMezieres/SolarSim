//
// Created by argo on 30/08/17.
//

#include "ui_DialogLoadSave.h"
#include "DialogLoadSave.h"
#include "src/Debug/Log.h"
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
#include <sys/socket.h>
#include <src/Core/String.h>
#include <src/Core/ParserObject.h>
#include <src/Core/ReadFile.h>
#include <src/Interface/MessageBox.h>
#include <src/Core/ReadDirectory.h>

DialogLoadSave::DialogLoadSave(MainWindow *mainWindow,QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogLoadSave) {
    ui->setupUi(this);

    this->setFixedSize(this->size());
    this->setWindowTitle("Save or load solar system");

    this->mainWindow=mainWindow;
    fullfillDefault();
    fullfillUser();
    defaultSystem=new DefaultSystem(mainWindow->getManagerRenderer(),mainWindow->getManagerTexture());


    ui->pushButton_2->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
    connect(ui->pushButton, SIGNAL(released()), this, SLOT(handleSave()));
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT (handleCharge()));
    connect(ui->pushButton_3, SIGNAL(released()), this, SLOT(handleRemove()));

    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem(QListWidgetItem * )));
    connect(ui->listWidget_2, SIGNAL(itemClicked(QListWidgetItem * )), this,
            SLOT(handleActivatedItem_2(QListWidgetItem * )));
}

void DialogLoadSave::fullfillDefault(){
    defaultSystem->fullfill(ui->listWidget);
}

void DialogLoadSave::fullfillUser(){
    defaultSystem->fullfillUser(ui->listWidget_2);
}

void DialogLoadSave::handleCharge() {
    mainWindow->getManagerRenderer()->removeAll();
    mainWindow->comboBox->removeAll();
    if(defaultChoice){
        defaultSystem->charge(choice->text().toStdString());
    }else{
        string file=String::replacePiece(choice->text().toStdString()," ","_");
        ParserObject::addFileToRender(file);
    }
    this->close();
}

bool DialogLoadSave::verifSave(){
    string line=ui->lineEdit->text().toStdString();
    if(line==""){
        MessageBox messageBox("Error","No name for file entered !",MessageBox::RED,MessageBox::WARNING_ICON);
        return false;
    }
    if(ReadDirectory::fileExist("../save",line)){
        MessageBox messageBox("Warning","Overwrite "+line+" ?",MessageBox::GREEN,MessageBox::WARNING_ICON,false);
        if(messageBox.exec()) return true;
        return false;
    }
    return true;
}

void DialogLoadSave::handleSave(){
    /*MessageBox messageBox("Test","test",MessageBox::NO_COLOR,MessageBox::NO_ICON,false);
    messageBox.addButton(QMessageBox::Ok);
    messageBox.addButton(QMessageBox::Cancel);
    if(messageBox.exec()) Log::debug("Ok");*/
    if(!verifSave()) return;
    string file=ui->lineEdit->text().toStdString();
    file=String::replacePiece(file," ","_");
    ReadFile::openFile("../save/"+file);
    ManagerRenderer * mr=mainWindow->getManagerRenderer();
    ListRenderer * lr=mr->getAllList();
    lr->iteratorFirst();
    while(lr->iteratorHasNext()){
        RenderObject *ro=lr->iteratorNext();
        string line=ro->childToString() + " " + ro->id + " ";
        line=line+ro->texture->id+" "+ro->sizeU+" "+to_string(ro->unitSize)+" ";
        line=line+ro->highU+" "+to_string(ro->unitHigh)+" "+ro->speedU+" ";
        line=line+to_string(ro->unitSpeed)+" "+ro->axisU+" "+ro->speedAU+" ";
        line=line+to_string(ro->unitSpeedA);
        if(ro->childToString()=="Sun") line=line+" "+to_string(ro->num_light);
        ReadFile::writeLine(line);
    }
    ReadFile::writeLine("EOF");
    ui->listWidget_2->clear();
    fullfillUser();
}

void DialogLoadSave::handleRemove(){
    MessageBox messageBox("Warning","Remove "+choice->text().toStdString()+" ?",MessageBox::RED,MessageBox::INFO_ICON,false);
    if(!messageBox.exec()) return;
    Log::debug("remove a file : "+choice->text().toStdString());
    string s="../save/"+choice->text().toStdString();
    remove(s.data());
    ui->listWidget_2->clear();
    fullfillUser();
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
}

void DialogLoadSave::handleActivatedItem(QListWidgetItem * item) {
    ui->pushButton_2->setEnabled(true);
    ui->pushButton_3->setEnabled(false);
    ui->listWidget_2->setCurrentRow(-1);
    choice=item;
    defaultChoice=true;
}

void DialogLoadSave::handleActivatedItem_2(QListWidgetItem * item){
    ui->pushButton_2->setEnabled(true);
    ui->pushButton_3->setEnabled(true);

    ui->listWidget->setCurrentRow(-1);
    choice=item;
    defaultChoice=false;
    ui->lineEdit->setText(item->text());

}

DialogLoadSave::~DialogLoadSave() {
    delete ui;
}