//
// Created by argo on 14/08/17.
//

#include "ui_DialogChargeNewTexture.h"
#include "DialogChargeNewTexture.h"
#include "src/Debug/Log.h"
#include "DialogChangeTexture.h"
#include <QtOpenGL/QGLFormat>
#include <QPushButton>
#include <src/Core/ReadDirectory.h>

DialogChargeNewTexture::DialogChargeNewTexture(GLWindow * glWindow,QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogChargeNewTexture) {
    ui->setupUi(this);
    this->setWindowTitle("Managing textures");
    this->glWindow=glWindow;
    this->setFixedSize(this->size());

    cancelButton = ui->buttonBox->button(QDialogButtonBox::Cancel);
    remove = ui->pushButton;
    refresh = ui->pushButton_3;
    tree= ui->pushButton_4;
    removeDef = ui->pushButton_2;

    connect(refresh, SIGNAL(released()), this, SLOT (handleRefresh()));
    connect(tree, SIGNAL(released()), this, SLOT (handleTree()));
    connect(remove, SIGNAL(released()), this, SLOT (handleRemove()));
    connect(removeDef, SIGNAL(released()), this, SLOT (handleRemoveDef()));

}

void DialogChargeNewTexture::handleRefresh() {
    Log::debug("Refresh");
    ReadDirectory::iteratorFirst("../textures");
    string file=ReadDirectory::iteratorNextS();
    string name;
    do{
        if(ReadDirectory::isFile(&file)) {
            name=ReadDirectory::nameWithoutExtension(file);
            if(glWindow->getManagerTexture()->getObjectTextureFromId(name)== nullptr)
                glWindow->getManagerTexture()->addTexture(name,file);
        }
        file=ReadDirectory::iteratorNextS();
    }while(ReadDirectory::iteratorHasNext());
}

void DialogChargeNewTexture::handleTree() {
    Log::debug("Tree");
}

void DialogChargeNewTexture::handleRemove() {
    Log::debug("Remove");
}

void DialogChargeNewTexture::handleRemoveDef() {
    Log::debug("Remove Def");
}


DialogChargeNewTexture::~DialogChargeNewTexture() {
    delete ui;
}
