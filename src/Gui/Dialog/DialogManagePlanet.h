//
// Created by argo on 15/08/17.
//

#ifndef SOLARSIM_DIALOGADDPLANET_H
#define SOLARSIM_DIALOGADDPLANET_H

/**********************************************
 *  /!\    /!\    /!\     /!\
 * YOU HAVE ADDED SOME PARAMETER
 * AND NOW YOU WANT SOME BUTTON TO INTERACT
 * -> READ THIS :
 * -ADD ALL THE BUTTON WITH QT CREATOR
 * -On DialogManagePlanet:
 * create the slot for button
 * and connect them
 * add the good verification
 * and modify the slot 'handleAddButton' or 'handleModifyButton'
 *
 * /!\    /!\    /!\     /!\
 */


#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <src/Gui/GLWindow.h>
#include <src/Gui/MainWindow.h>

namespace Ui {
    class DialogManagePlanet;
}

class DialogManagePlanet : public QDialog
{
Q_OBJECT

public:

    enum IDComboBox{CPlanet,CSun};
    explicit DialogManagePlanet(MainWindow *mainWindow,GLWindow *glWindow,QWidget *parent = 0);

    GLWindow *glWindow;
    MainWindow * mainWindow;

    void refreshListModel();
    void refreshListTexture();

    ~DialogManagePlanet();

private:
    Ui::DialogManagePlanet *ui;
    string planetToRemove;
    Texture *textureToApply,*textureToModify;
    RenderObject *objectToModify;

    void getTexture2FromList(string id);
    void getTexture3FromList(string id);


    void verifIdPlanet(List<int> *list);
    void verifIdPlanet_2(List<int> *list);
    void verifTexture(List<int> *list);
    void verifFloat(List<int> *list);
    void verifFloat_2(List<int> *list);
    int verifAddPlanet();
    int verifModifyPlanet();
    void messageBox(string t, string s);
    void activateModify(RenderObject::Child child);

    QString text_1,text_3,text_4,text_6,text_7,text_8,text_9,text_10,text_11,text_12;
    IDComboBox idComboBox=CPlanet;


protected slots:


    void handleAddButton();
    void handleRemoveButton();
    void handleModifyButton();
    void handleActivatedItem(QListWidgetItem *item);
    void handleActivatedItem2(QListWidgetItem *item);
    void handleActivatedItem3(QListWidgetItem *item);
    void handleActivatedItem_3(QListWidgetItem * item);
    void handleChangeText_1(const QString &text);
    void handleChangeText_3(const QString &text);
    void handleChangeText_4(const QString &text);
    void handleChangeText_6(const QString &text);
    void handleChangeText_7(const QString &text);
    void handleChangeText_8(const QString &text);
    void handleChangeText_9(const QString &text);
    void handleChangeText_10(const QString &text);
    void handleChangeText_11(const QString &text);
    void handleChangeText_12(const QString &text);
    void handleChangeComboBox_4(int index);


};

#endif //SOLARSIM_DIALOGADDPLANET_H
