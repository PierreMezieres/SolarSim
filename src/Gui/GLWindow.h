//
// Created by argo on 07/06/17.
//

#ifndef SOLARSIM_GLWINDOW_H
#define SOLARSIM_GLWINDOW_H

#include "GLWidget.h"

#include <QTime>
#include <src/Engine/Renderer/ListRenderer.h>
#include <src/Engine/Renderer/Element/Planet.h>
#include <src/Engine/Renderer/ManagerRenderer.h>
#include <src/Engine/ManagerTexture.h>

class GLWindow : public GLWidget {
    Q_OBJECT
public:

    bool play;
    GLfloat time,lastTime,timeValue;

    void debugAjoutPlanete();

    explicit GLWindow(QWidget *parent = 0);

    void InitializationManagerTexture();
    void initializeGL();

    void resizeGL(int width, int height);

    void paintGL();

    void loadTexture(QString textureName, GLuint *texture);

    static glm::vec3 getLookPlanet();
    static void setLookPlanet(float x, float y, float z);
    void setLookPlanetId(std::string text);
    static string getIdLookPlanet();
    static void setIdLookPlanet(std::string text);

    ManagerTexture * getManagerTexture();
    ManagerRenderer * getManagerRenderer();

private:

    static glm::vec3 lookPlanet;
    static string idLookPlanet;

    //Shader *shader = NULL;
    GLuint earth, ciel, sunT;
    ManagerTexture managerTexture;

    ManagerRenderer managerRenderer;
    //ListRenderer listRenderer;




};

#endif //SOLARSIM_GLWINDOW_H
