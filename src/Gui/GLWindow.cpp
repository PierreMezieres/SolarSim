//
// Created by argo on 07/06/17.
//

#include "GLWindow.h"
#include "../Debug/Log.h"

#include <GL/glu.h>
#include <QGLWidget>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <src/Engine/Renderer/Element/Sun.h>
#include <src/Engine/Renderer/Element/Planet.h>
#include <src/Core/ReadDirectory.h>
#include <src/Core/Light.h>
#include <src/Core/ParserObject.h>

//Initializing static member
glm::vec3 GLWindow::lookPlanet(0,0,0);
string GLWindow::idLookPlanet= string("Earth");

GLWindow::GLWindow(QWidget *parent)
        : GLWidget(500, parent, (char *) "SolarSim") {
}

void GLWindow::InitializationManagerTexture(){
    ReadDirectory::iteratorFirst("../textures");
    string file=ReadDirectory::iteratorNextS();
    string name;
    do{
        if(ReadDirectory::isFile(&file)) {
            name=ReadDirectory::nameWithoutExtension(file);
            managerTexture.addTexture(name,file);
        }
        file=ReadDirectory::iteratorNextS();
    }while(ReadDirectory::iteratorHasNext());
    play=true;
    lastTime=0;
    time=0;
}

void GLWindow::debugAjoutPlanete(){


   /* managerTexture.addTexture("Earth","EarthLow.jpg");
    managerTexture.addTexture("Sun","sun.bmp");
    managerTexture.addTexture("Sky","Star.jpg");
*/
    Sun *sun=new Sun(0,"Soleil",managerTexture.getObjectTextureFromId("Sun"),"1",0,"0",0,"0",0,"0","0",0);
    sun->addPosition(0,0,0);

    Planet *planet=new Planet("Earth",managerTexture.getObjectTextureFromId("Earth"),"0.4",0,"5",0,"10",0,"24","0",0);
    Planet *planet2=new Planet("Earth2",managerTexture.getObjectTextureFromId("Earth"),"0.5",0,"7",0,"16",0,"0","0",0);
    Planet *sky= new Planet("Sky",managerTexture.getObjectTextureFromId("Star"),"50",0,"0",0,"0",0,"0","0",0);
    /*Sun *sun=new Sun("Soleil",managerTexture.getObjectTextureFromId("Sun"),700,"700",2);
    sun->addPosition(0,0,0);

    Planet *planet=new Planet("Earth",managerTexture.getObjectTextureFromId("Earth"),6,"6",2,1,"1",0,0.3,"0.3",1);*/

    managerRenderer.add(planet,true,true);
    managerRenderer.add(sun,true,true);
    managerRenderer.add(sky,true,false);
    managerRenderer.add(planet2,true,true);
}

void GLWindow::initializeGL() {
    Log::debug("Initialize");


    // Texture
    glEnable(GL_TEXTURE_2D);
    InitializationManagerTexture();

    //Light
    glEnable(GL_LIGHTING);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);

    ParserObject::addManagerTexture(getManagerTexture());
    ParserObject::addManagerRenderer(getManagerRenderer());
   // debugAjoutPlanete(); // /!\ warning /!\


}

void GLWindow::resizeGL(int width, int height) {
    Log::debug("Resize");
    if (height == 0)
        height = 1;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat) width / (GLfloat) height, 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void GLWindow::paintGL() {

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        camera.look();

    glRotatef(90, -1, 0, 0);

    lastTime=timeValue;
    timeValue = QTime().currentTime().msecsSinceStartOfDay() * 0.001f;

    if(play){
        time=time+(timeValue-lastTime);
        managerRenderer.calculatePosition(time);
    }


        glm::vec3 tran = GLWindow::getLookPlanet();
        glTranslatef(-tran.x, -tran.y, -tran.z);
        managerRenderer.renderList();



    /*GLfloat position[4];
    position[0]=0; position[1]=0; position[2]=1;
    position[3]=1;
    glLightfv(GL_LIGHT0, GL_POSITION, position);
*/
    /*GLUquadric * params = gluNewQuadric();
    gluQuadricTexture(params, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D, earth);
    glRotatef(90,-1,0,0);

    gluSphere(params, 0.4, 20, 20);

    glBindTexture(GL_TEXTURE_2D, ciel);
    gluSphere(params, 50, 20, 20);

    gluDeleteQuadric(params);*/
}

void GLWindow::loadTexture(QString textureName, GLuint *texture) {
    QImage qim_Texture;
    QImage qim_TempTexture;
    qim_TempTexture.load(textureName);
    qim_Texture = QGLWidget::convertToGLFormat( qim_TempTexture );
    glGenTextures( 1, texture );
    glBindTexture( GL_TEXTURE_2D, *texture );
    glTexImage2D( GL_TEXTURE_2D, 0, 3, qim_Texture.width(), qim_Texture.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, qim_Texture.bits() );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
}

glm::vec3 GLWindow::getLookPlanet(){
    return lookPlanet;
}

void GLWindow::setLookPlanet(float x, float y, float z){
    lookPlanet = glm::vec3(x,y,z);
}

void GLWindow::setIdLookPlanet(std::string text){
    idLookPlanet=text;
}

void GLWindow::setLookPlanetId(std::string text){
    idLookPlanet=text;
    RenderObject * ro=managerRenderer.getRenderList()->getROFromId(text);
    if(ro== nullptr) setLookPlanet(0,0,0);
    else setLookPlanet(ro->position[0],ro->position[1],ro->position[2]);
}

string GLWindow::getIdLookPlanet(){
    return idLookPlanet;
}


ManagerTexture * GLWindow::getManagerTexture(){
    return &managerTexture;
}

ManagerRenderer * GLWindow::getManagerRenderer(){
    return &managerRenderer;
}