#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <src/Interface/ComboBox.h>
#include <src/Engine/Renderer/ManagerRenderer.h>
#include <src/Engine/ManagerTexture.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void closeEvent(QCloseEvent *event) override;
    static void addSwitchPlanet(std::string st);
    void removeSwitchPlanet(std::string st);

    ManagerRenderer * getManagerRenderer();
    ManagerTexture * getManagerTexture();

    static ComboBox *comboBox;

        ~MainWindow();

private:
    Ui::MainWindow *ui;

protected slots:

    void handleChangeTexture();

    void handleChangeComboBox(const QString & text);
    void handleAddPlanet();
    void handleLoadSave();
    void handlePlayButton();
    void handleChangeSpinBox(int i);
    void handleChangeSpinBox2(int i);
    void handleChangeSpinBox3(int i);
    void handleChangeSpinBox4(int i);
    void handleChangeComboBox2(int i);
    void handleChangeComboBox3(int i);
    void handleChangeComboBox4(int i);
    void handleChangeComboBox5(int i);

};

#endif // MAINWINDOW_H
