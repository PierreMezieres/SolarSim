

#include "ui_MainWindow.h"
#include "MainWindow.h"
#include <src/Gui/Dialog/DialogChangeTexture.h>
#include "../Debug/Log.h"
#include <QtOpenGL/QGLFormat>
#include <src/Gui/Dialog/DialogManagePlanet.h>
#include <src/Gui/Dialog/DialogLaunch.h>
#include <src/Gui/Dialog/DialogLoadSave.h>

ComboBox * MainWindow::comboBox;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("SolarSim");
    ui->dockWidget_2->setFixedWidth(120);
    ui->toolButton->setIcon(QIcon("../Icon/Pause.ico"));
    connect(ui->pushButton, SIGNAL(released()), this, SLOT (handleChangeTexture()));
    connect(ui->pushButton_3, SIGNAL(released()), this, SLOT(handleLoadSave()));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT (handleChangeComboBox(const QString)));
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT (handleAddPlanet()));
    connect(ui->toolButton, SIGNAL(released()), this, SLOT(handlePlayButton()));
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(handleChangeSpinBox(int)));
    connect(ui->spinBox_2, SIGNAL(valueChanged(int)), this, SLOT(handleChangeSpinBox2(int)));
    connect(ui->spinBox_3, SIGNAL(valueChanged(int)), this, SLOT(handleChangeSpinBox3(int)));
    connect(ui->spinBox_4, SIGNAL(valueChanged(int)), this, SLOT(handleChangeSpinBox4(int)));
    connect(ui->comboBox_2, SIGNAL(currentIndexChanged(int)),this, SLOT(handleChangeComboBox2(int)));
    connect(ui->comboBox_3, SIGNAL(currentIndexChanged(int)),this, SLOT(handleChangeComboBox3(int)));
    connect(ui->comboBox_4, SIGNAL(currentIndexChanged(int)),this, SLOT(handleChangeComboBox4(int)));
    connect(ui->comboBox_5, SIGNAL(currentIndexChanged(int)),this, SLOT(handleChangeComboBox5(int)));

    comboBox=ui->comboBox;
    /*QGLFormat glFormat;
    glFormat.setVersion(3, 3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);
    ui->openGLWidget->setFormat(glFormat);*/
}

void MainWindow::closeEvent(QCloseEvent *event) {
    Log::print("\n");
    Log::success(" A Bientot \n");
}

void MainWindow::handleChangeTexture(){
    DialogChangeTexture dialog(ui->openGLWidget);
    //dialog.show();
    dialog.exec();
    comboBox->setCurrentIndex(comboBox->getIndex(ui->openGLWidget->getIdLookPlanet()));
}

void MainWindow::addSwitchPlanet(std::string st){
    QString ch = QString::fromStdString(st);
    comboBox->addItem(ch);
}

void MainWindow::removeSwitchPlanet(std::string st){
    comboBox->removeItem(comboBox->getIndex(st));
}

void MainWindow::handleChangeComboBox(const QString & text){
    string st = text.toStdString();
   // ui->openGLWidget->setIdLookPlanet(st);
    ui->openGLWidget->setLookPlanetId(st);
}

void MainWindow::handleAddPlanet(){
    DialogManagePlanet dialogManagePlanet(this,ui->openGLWidget);
    dialogManagePlanet.exec();
    comboBox->setCurrentIndex(comboBox->getIndex(ui->openGLWidget->getIdLookPlanet()));
}

void MainWindow::handlePlayButton(){
    if(ui->openGLWidget->play){
        ui->openGLWidget->play=false;
        ui->toolButton->setIcon(QIcon("../Icon/Play.png"));
    }else{
        ui->openGLWidget->play=true;
        ui->toolButton->setIcon(QIcon("../Icon/Pause.ico"));
    }
}

void MainWindow::handleChangeSpinBox(int i){
    Log::intValue("Valeur Change:",i);
    Unit::size.val=i;
    ui->openGLWidget->getManagerRenderer()->updateSize();

}

void MainWindow::handleChangeSpinBox2(int i){
    Log::intValue("Valeur Change:",i);
    Unit::high.val=i;
    ui->openGLWidget->getManagerRenderer()->updateHigh();
}

void MainWindow::handleChangeSpinBox3(int i){
    Log::intValue("Valeur Change:",i);
    Unit::speed.val=i;
    ui->openGLWidget->getManagerRenderer()->updateSpeed();
}

void MainWindow::handleChangeSpinBox4(int i){
    Log::intValue("Valeur Change:",i);
    Unit::speedA.val=i;
    ui->openGLWidget->getManagerRenderer()->updateSpeedA();
}

void MainWindow::handleChangeComboBox2(int i){
    Log::intValue("Valeur Change:",i);
    Unit::size.dist=ui->comboBox_2->getUnitIndex();
    ui->openGLWidget->getManagerRenderer()->updateSize();
}

void MainWindow::handleChangeComboBox3(int i){
    Log::intValue("Valeur Change:",i);
    Unit::high.dist=ui->comboBox_3->getUnitIndex();
    ui->openGLWidget->getManagerRenderer()->updateHigh();
}

void MainWindow::handleChangeComboBox4(int i){
    Log::intValue("Valeur Change:",i);
    Unit::speed.time=ui->comboBox_4->getUnitIndex2();
    ui->openGLWidget->getManagerRenderer()->updateSpeed();
}

void MainWindow::handleChangeComboBox5(int i){
    Log::intValue("Valeur Change:",i);
    Unit::speedA.time=ui->comboBox_5->getUnitIndex2();
    ui->openGLWidget->getManagerRenderer()->updateSpeedA();
}

ManagerRenderer * MainWindow::getManagerRenderer(){
    return ui->openGLWidget->getManagerRenderer();
}
ManagerTexture * MainWindow::getManagerTexture(){
    return ui->openGLWidget->getManagerTexture();
}

void MainWindow::handleLoadSave(){
    DialogLoadSave dialogLoadSave(this);
    dialogLoadSave.exec();
}

MainWindow::~MainWindow()
{
    delete ui;
}

