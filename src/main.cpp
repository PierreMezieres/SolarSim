
#include <iostream>
#include <stdio.h>

#include <QApplication>
#include <QtWidgets>
#include "Gui/MainWindow.h"
#include <src/Gui/Dialog/DialogLaunch.h>
#include "Debug/Debug.h"


int main(int argc, char *argv[]) {
    QApplication app(argc, argv);


    MainWindow window;
    window.show();
    window.setVisible(false);

    DialogLaunch dialogLaunch(&window);
    dialogLaunch.show();




    //Test when starting !
    Debug::verificationStart();

    return app.exec();
}