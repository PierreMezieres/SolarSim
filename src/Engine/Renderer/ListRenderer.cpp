//
// Created by argo on 12/06/17.
//

#include <src/Gui/GLWindow.h>
#include <src/Engine/Renderer/Element/Sun.h>
#include <src/Debug/Log.h>
#include <src/Interface/ComboBox.h>
#include "ListRenderer.h"
#include "math.h"

ListRenderer::ListRenderer(){
    startNode = nullptr;
    iteratorFirst();
}

void ListRenderer::add(RenderObject * renderObject){
    NodeRenderer * create = new NodeRenderer(renderObject, startNode);
    startNode = create;
    iteratorFirst();
}

void ListRenderer::remove(std::string id){
    iteratorFirst();
    NodeRenderer *previous= nullptr,*actual=iterator;
    RenderObject *ro;
    while(iteratorHasNext()){
        ro=iterator->getRenderObject();
        if(ro->id==id){
            ro->~RenderObject();
            if(actual==startNode){
                startNode=startNode->nextNode;
            }else{
                if(previous==nullptr) startNode== nullptr;
                else previous->nextNode=actual->nextNode;
            }
            return;
        }
        previous=iterator;
        iteratorNext();
        actual=iterator;

    }
}

void ListRenderer::eraseAll(){
    startNode = nullptr;
    iteratorFirst();
}

void ListRenderer::iteratorFirst(){
    iterator = startNode;
}

int ListRenderer::iteratorHasNext(){
    return iterator != nullptr;
}

RenderObject * ListRenderer::iteratorNext(){
    if(iteratorHasNext()){
        NodeRenderer * nodeRenderer = iterator;
        iterator = nodeRenderer->getNextNode();
        return nodeRenderer->getRenderObject();
    }
    return nullptr;
}

void ListRenderer::renderList(){
    iteratorFirst();
    while(iteratorHasNext()){
        iterator->getRenderObject()->render();
        iteratorNext();
    }

}

RenderObject * ListRenderer::getROFromId(std::string id){
    iteratorFirst();
    while(iteratorHasNext()){
        RenderObject *ro=iterator->getRenderObject();
        if(ro->id==id) return ro;
        iteratorNext();
    }
    return nullptr;
}


void ListRenderer::calculatePosition(GLfloat time){
    iteratorFirst();
    while(iteratorHasNext()){
        RenderObject *ro=iterator->getRenderObject();
        ro->calculatePosition(time);
        if(ro->id == GLWindow::getIdLookPlanet()){
            GLWindow::setLookPlanet(ro->position[0], ro->position[1], ro->position[2]);
        }
        iteratorNext();
    }
}