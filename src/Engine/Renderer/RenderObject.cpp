//
// Created by argo on 13/06/17.
//

/**********************************************
 *  /!\    /!\    /!\     /!\
 * WANT TO ADD SOME PARAMETER FOR RO (ex: param)
 * -> READ THIS :
 * -On RenderObject:
 * Add the parameter and write the function 'changeParam'
 * -On ManagerRenderer:
 * write the function 'updateParam()'
 * -On the element inherited from RO:
 * add on the constructor the element
 * and don't forget to call 'changeParam' on the constructor
 *
 * You want to add some button to add the parameter,
 * go to class: src/Gui/Dialog/DialogManagePlanet.cpp
 *
 * /!\    /!\    /!\     /!\
 */


#include <src/Debug/Log.h>
#include <src/Core/Unit.h>
#include "RenderObject.h"
#include <src/Core/Unit.h>
#include <QtCore/QString>
#include <src/Interface/ComboBox.h>
#include <cmath>

RenderObject::RenderObject(Child child,std::string id,Texture * texture, float size, float high, float speed, glm::vec3 pos) {
    this->id=id;
    this->size=size;
    this->speed=speed;
    this->high=high;
    this->position[0]=pos.x;
    this->position[1]=pos.y;
    this->position[2]=pos.z;
    this->position[3]=1;
    this->texture=texture;
    this->child=child;
    this->valueAxis=0;
}

void RenderObject::addPosition(GLfloat x, GLfloat y, GLfloat z){
    position[0]=x;
    position[1]=y;
    position[2]=z;
    position[3]=1;
}

float RenderObject::conversionInput(float val,Unit::Unit_Term unit,float unitGlobal){
    float glob=unitGlobal;
    float res=val;
    switch(unit){
        case Unit::UA: res=res*150000; break;
        case Unit::Km: res=res/1000; break;
        case Unit::TKm: res=res; break;
        case Unit::MKm: res=res*1000; break;
        case Unit::BKm: res=res*1000000; break;
        default: break;
    }
    return res/glob;
}

float RenderObject::conversionInputTime(float val,Unit::Unit_Time unit,float unitGlobal){
    float glob=unitGlobal;
    float res;
    if(val>0.) res=1./val;
    else return 0;
    switch(unit){
        case Unit::SEC: break;
        case Unit::MIN: res=res/60; break;
        case Unit::HOU: res=res/60/60; break;
        case Unit::DAY: res=res/60/60/24; break;
        case Unit::MON: res=res/60/60/24/30; break;
        case Unit::YEA: res=res/60/60/24/365; break;
        default: break;
    }
    return res*glob;
}

void RenderObject::changeSize(string size,int unitSize){
    Unit::Unit_Term code=ComboBox::getUnitIndex(unitSize);
    this->size=conversionInput(QString::fromStdString(size).toFloat(),code,Unit::getSizeUnit());
    this->sizeU=size;
    this->unitSize=unitSize;
}

void RenderObject::changeHigh(string high,int unitHigh){
    Unit::Unit_Term code=ComboBox::getUnitIndex(unitHigh);
    this->high=conversionInput(QString::fromStdString(high).toFloat(),code,Unit::getHighUnit());
    this->highU=high;
    this->unitHigh=unitHigh;
}

void RenderObject::changeSpeed(string speed,int unitSpeed){
    Unit::Unit_Time code=ComboBox::getUnitIndex2(unitSpeed);
    this->speed=conversionInputTime(QString::fromStdString(speed).toFloat(),code,Unit::getSpeedUnit());
    this->speedU=speed;
    this->unitSpeed=unitSpeed;
}

void RenderObject::changeAxisTilt(string axis){
    this->axis=QString::fromStdString(axis).toFloat();
    this->axisU=axis;
}

void RenderObject::changeSpeedAxis(string speedA,int unitSpeedA){
    Unit::Unit_Time code=ComboBox::getUnitIndex2(unitSpeedA);
    float speed=QString::fromStdString(speedA).toFloat();
    this->speedA=conversionInputTime(speed,code,Unit::getSpeedAUnit());
    Log::floatValue("speed",this->speedA);
    this->speedAU=speedA;
    this->unitSpeedA=unitSpeedA;
}

void RenderObject::render(){
    Log::warning("Possibly bug if you read this line");
    Log::warning("Abstract Render object does not have to be render");
}


float moduloFloat(float a,float b){
    float c=a;
    while(c>b) c=c-b;
    return c;
}

void RenderObject::calculatePosition(GLfloat time){
    float a=time*this->speed*M_PI*2;
    this->position[0]=this->high*sin(a);
    this->position[1]=this->high*cos(a);
    a=time*360*this->speedA;
    this->valueAxis=moduloFloat(a,360);
}

void RenderObject::debug(){
    Log::print("\n\n");
    Log::debug("Content Render Object : " + this->id);
    Log::debug("Name Texture : " + this->texture->id);
    Log::floatValue("Size",this->size);
    float si=this->size;//QString::fromStdString(this->sizeU).toFloat();
    Log::floatValue("SizeReal ",si*Unit::getKmFromUnit(ComboBox::getUnitIndex(this->unitSize))/150000000);
    Log::floatValue("High",this->high);
    Log::floatValue("Speed",this->speed);
    Log::floatValue("Speed Axis",this->speedA);

    Log::print("\n\n");
}


string RenderObject::childToString(){
    if(this->child==Sun) return "Sun";
    if(this->child==Planet) return "Planet";
}

RenderObject::~RenderObject(){
    if(child==Sun){
        Log::debug("Destroy a sun");
        glDisable(light);
    }

};