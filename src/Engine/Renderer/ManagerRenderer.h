//
// Created by argo on 08/08/17.
//

/******************************************************
 * ManagerRenderer, manage all the kind of listRenderer
 * necessary
 * -> allL : list of all the RenderObject
 * -> renderL : list of the RO to render
 * -> positionL : list of RO to recalculate position
 *
 * Using different list, instead of just boolean
 * allows to gain some time (I think)
 ******************************************************/

#ifndef SOLARSIM_MANAGERRENDERER_H
#define SOLARSIM_MANAGERRENDERER_H

#include "ListRenderer.h"

class ManagerRenderer{

public:
    void add(RenderObject * renderObject, bool render=false, bool position=false);
    void addAll(RenderObject * renderObject);
    void addRender(RenderObject * renderObject);
    void addPosition(RenderObject * renderObject);
    void remove(string id);
    void removeAll();

    void calculatePosition(float timeValue);
    void renderList();
    ListRenderer * getRenderList();
    ListRenderer * getAllList();
    int getLightAvailable(GLenum *light= nullptr);


    //Update Size,High,Speed of all the RenderObject
    void updateSize();
    void updateHigh();
    void updateSpeed();
    void updateSpeedA();


private:
    ListRenderer allL, renderL, positionL;
};

#endif //SOLARSIM_MANAGERRENDERER_H
