//
// Created by argo on 30/08/17.
//

#include <src/Engine/Renderer/Element/Sun.h>
#include <src/Engine/Renderer/Element/Planet.h>
#include <src/Core/ReadDirectory.h>
#include <src/Core/String.h>
#include "DefaultSystem.h"

DefaultSystem::DefaultSystem(ManagerRenderer *managerRenderer, ManagerTexture *managerTexture){
    this->managerRenderer=managerRenderer;
    this->managerTexture=managerTexture;
}

void DefaultSystem::fullfill(QListWidget *list){
    list->addItem("Solar System Illustration");
    list->addItem("Earth Illustration");
    list->addItem("Earth Real");
    list->addItem("Debug");
}

void DefaultSystem::fullfillUser(QListWidget *list){
    ReadDirectory::iteratorFirst("../save");
    string s;
    while(ReadDirectory::iteratorNextS(&s)){
        s=String::replacePiece(s,"_"," ");
        if(ReadDirectory::isFile(&s))list->addItem(QString::fromStdString(s));
    }
    /*string s=ReadDirectory::iteratorNextS();
    do{
        s=String::replacePiece(s,"_"," ");
        if(ReadDirectory::isFile(&s))list->addItem(QString::fromStdString(s));
        s=ReadDirectory::iteratorNextS();
    }while(ReadDirectory::iteratorHasNext());*/
}

void DefaultSystem::charge(string s){
    if(s=="Debug") debug();
    if(s=="Solar System Illustration") solarSystemIllustration();
    if(s=="Earth Illustration") earthIllustration();
    if(s=="Earth Real") earthReal();
}

void DefaultSystem::debug(){
    Sun *sun=new Sun(0,"Soleil",managerTexture->getObjectTextureFromId("Sun"),"1",0,"0",0,"0",0,"0","0",0);
    sun->addPosition(0,0,0);

    Planet *planet=new Planet("Earth",managerTexture->getObjectTextureFromId("Earth"),"0.4",0,"5",0,"10",0,"24","0",0);
    Planet *planet2=new Planet("Earth2",managerTexture->getObjectTextureFromId("Earth"),"0.5",0,"7",0,"16",0,"0","0",0);
    Planet *sky= new Planet("Sky",managerTexture->getObjectTextureFromId("Star"),"50",0,"0",0,"0",0,"0","0",0);
    /*Sun *sun=new Sun("Soleil",managerTexture.getObjectTextureFromId("Sun"),700,"700",2);
    sun->addPosition(0,0,0);

    Planet *planet=new Planet("Earth",managerTexture.getObjectTextureFromId("Earth"),6,"6",2,1,"1",0,0.3,"0.3",1);*/

    managerRenderer->add(planet,true,true);
    managerRenderer->add(sun,true,true);
    managerRenderer->add(sky,true,false);
    managerRenderer->add(planet2,true,true);
}

void DefaultSystem::solarSystemIllustration(){
    Sun *sun=new Sun(0,"Soleil",managerTexture->getObjectTextureFromId("Sun"),"1",0,"0",0,"0",0,"0","0",0);
    sun->addPosition(0,0,0);

    Texture * earthT=managerTexture->getObjectTextureFromId("Earth");
    Texture * mercuryT=managerTexture->getObjectTextureFromId("Mercury");
    Texture * venusT=managerTexture->getObjectTextureFromId("Venus");
    Texture * marsT=managerTexture->getObjectTextureFromId("Mars");
    Texture * jupiterT=managerTexture->getObjectTextureFromId("Jupiter");
    Texture * saturnT=managerTexture->getObjectTextureFromId("Saturn");
    Texture * uranusT=managerTexture->getObjectTextureFromId("Uranus");
    Texture * neptuneT=managerTexture->getObjectTextureFromId("Neptune");
    Texture * plutoT=managerTexture->getObjectTextureFromId("Pluto");

    Planet *planet=new Planet("Earth",earthT,"0.4",0,"5",0,"1",5,"24","1",3);
    Planet *mercury=new Planet("Mercury",mercuryT,"0.4",0,"3",0,"87",3,"0","88",3);
    Planet *venus=new Planet("Venus",venusT,"0.4",0,"4",0,"224",3,"177","243",3);
    Planet *mars=new Planet("Mars",marsT,"0.4",0,"6",0,"686",3,"25","1",3);
    Planet *jupiter=new Planet("Jupiter",jupiterT,"0.4",0,"7",0,"12",5,"3","10",2);
    Planet *saturn=new Planet("Saturn",saturnT,"0.4",0,"8",0,"30",5,"26","11",2);
    Planet *uranus=new Planet("Uranus",uranusT,"0.4",0,"9",0,"84",5,"97","17",2);
    Planet *neptune=new Planet("Neptune",neptuneT,"0.4",0,"10",0,"164",5,"30","16",2);
    Planet *pluto=new Planet("Pluto",plutoT,"0.4",0,"11",0,"248",5,"0","6",3);

    Planet *sky= new Planet("Sky",managerTexture->getObjectTextureFromId("Star"),"50",0,"0",0,"0",0,"0","0",0);

    managerRenderer->add(planet,true,true);
    managerRenderer->add(sun,true,true);
    managerRenderer->add(mercury,true,true);
    managerRenderer->add(venus,true,true);
    managerRenderer->add(mars,true,true);
    managerRenderer->add(jupiter,true,true);
    managerRenderer->add(saturn,true,true);
    managerRenderer->add(uranus,true,true);
    managerRenderer->add(neptune,true,true);
    managerRenderer->add(pluto,true,true);
    managerRenderer->add(sky,true,false);
}

void DefaultSystem::earthIllustration(){
    Sun *sun=new Sun(0,"Soleil",managerTexture->getObjectTextureFromId("Sun"),"1",0,"0",0,"0",0,"0","0",0);
    sun->addPosition(0,0,0);

    Texture * earthT=managerTexture->getObjectTextureFromId("Earth");

    Planet *planet=new Planet("Earth",earthT,"0.4",0,"3",0,"1",5,"24","1",3);
    Planet *sky= new Planet("Sky",managerTexture->getObjectTextureFromId("Star"),"50",0,"0",0,"0",0,"0","0",0);

    managerRenderer->add(planet,true,true);
    managerRenderer->add(sun,true,true);
    managerRenderer->add(sky,true,false);
}

void DefaultSystem::earthReal(){
    Sun *sun=new Sun(0,"Soleil",managerTexture->getObjectTextureFromId("Sun"),"700",2,"0",0,"0",0,"0","0",0);
    sun->addPosition(0,0,0);

    Texture * earthT=managerTexture->getObjectTextureFromId("Earth");

    Planet *planet=new Planet("Earth",earthT,"6",2,"1",0,"1",5,"24","1",3);

    managerRenderer->add(planet,true,true);
    managerRenderer->add(sun,true,true);
}