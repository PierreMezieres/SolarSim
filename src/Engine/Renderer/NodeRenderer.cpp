//
// Created by argo on 12/06/17.
//

#include "NodeRenderer.h"

NodeRenderer::NodeRenderer(RenderObject * renderObject, NodeRenderer * nextNode) {
    this->renderObject = renderObject;
    this->nextNode = nextNode;
}

void NodeRenderer::addRenderObject(RenderObject * renderObject){
    this->renderObject = renderObject;
}
RenderObject * NodeRenderer::getRenderObject(){
    return renderObject;
}

void NodeRenderer::addNext(NodeRenderer * nextNode) {
    this->nextNode = nextNode;
}

NodeRenderer * NodeRenderer::getNextNode(){
    return nextNode;
}
