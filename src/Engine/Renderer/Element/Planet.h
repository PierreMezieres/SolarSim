//
// Created by argo on 21/07/17.
//

/******************************************************
 * Class inherited of RenderObject
 * -> It's a Planet to render
 ******************************************************/

#ifndef SOLARSIM_PLANET_H
#define SOLARSIM_PLANET_H

#include "src/Engine/Renderer/RenderObject.h"

class Planet : public RenderObject
{
public :
    explicit Planet(std::string id, Texture * texture,
                    string size, int unitSize,
                    string high, int unitHigh,
                    string speed, int unitSpeed,
                    string axis,
                    string speedA, int unitSpeedA);

    void render() override;
};

#endif //SOLARSIM_PLANET_H
