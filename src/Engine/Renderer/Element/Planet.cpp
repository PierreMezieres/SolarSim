//
// Created by argo on 21/07/17.
//

#include <src/Debug/Log.h>
#include <glm/vec3.hpp>
#include <src/Gui/GLWindow.h>
#include <GL/glu.h>
#include "Planet.h"

Planet::Planet(std::string id, Texture * texture,
               string size, int unitSize,
               string high, int unitHigh,
               string speed, int unitSpeed,
               string axis,
               string speedA, int unitSpeedA) : RenderObject(RenderObject::Planet,id,texture){
    Log::debug("Construct Planet : " + id);
    changeSize(size,unitSize);
    changeHigh(high,unitHigh);
    changeSpeed(speed,unitSpeed);
    changeAxisTilt(axis);
    changeSpeedAxis(speedA,unitSpeedA);
}

void Planet::render(){
    GLUquadric * params = gluNewQuadric();
    gluQuadricTexture(params, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D, texture->texture);

    glPushMatrix();
    glTranslatef(position[0],position[1],position[2]);
    glRotatef(valueAxis,0,0,1);
    glRotatef(axis,1,0,0);

    gluSphere(params, size, 20, 20);

    glPopMatrix();

    gluDeleteQuadric(params);

}