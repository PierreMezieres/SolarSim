//
// Created by argo on 21/07/17.
//

/******************************************************uygfyd
 * Class inherited of RenderObject
 * -> It's a Sun to render
 ******************************************************/

#ifndef SOLARSIM_SUN_H
#define SOLARSIM_SUN_H

#include "src/Engine/Renderer/RenderObject.h"

class Sun : public RenderObject
{
public :
    explicit Sun(int i,std::string id, Texture * texture,
                 string size, int unitSize,
                 string high, int unitHigh,
                 string speed, int unitSpeed,
                 string axis,
                 string speedA, int unitSpeedA);

    void render() override;


};

#endif //SOLARSIM_SUN_H
