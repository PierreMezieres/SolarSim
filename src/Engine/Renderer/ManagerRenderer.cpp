//
// Created by argo on 08/08/17.
//

#include <src/Gui/MainWindow.h>
//#include <src/Core/List.h>
#include <src/Core/Light.h>
#include "ManagerRenderer.h"

void ManagerRenderer::add(RenderObject * renderObject, bool render, bool position){
    allL.add(renderObject);
    if(render) {
        renderL.add(renderObject);
        MainWindow::addSwitchPlanet(renderObject->id);
    }
    if(position) positionL.add(renderObject);
}

void ManagerRenderer::addAll(RenderObject * renderObject){
    allL.add(renderObject);
}

void ManagerRenderer::addRender(RenderObject * renderObject){
    renderL.add(renderObject);
}

void ManagerRenderer::addPosition(RenderObject * renderObject){
    positionL.add(renderObject);
}

void ManagerRenderer::remove(string id){
    allL.remove(id);
    renderL.remove(id);
    positionL.remove(id);
}

void ManagerRenderer::removeAll(){
    allL.eraseAll();
    renderL.eraseAll();
    positionL.eraseAll();
}

void ManagerRenderer::calculatePosition(float timeValue){
    positionL.calculatePosition(timeValue);
}

void ManagerRenderer::renderList(){
    renderL.renderList();
}

ListRenderer * ManagerRenderer::getRenderList(){
    return &renderL;
}

ListRenderer * ManagerRenderer::getAllList(){
    return &allL;
}


int ManagerRenderer::getLightAvailable(GLenum *light){
    allL.iteratorFirst();
    GLenum newLight;
    List<GLenum> list;
    while(allL.iteratorHasNext()){
        RenderObject * ro=allL.iteratorNext();
        if(ro->child==RenderObject::Sun) {
            list.add(ro->light);
        }
    }
    for(int i=0;i<Light::numberLight();i++){
        newLight=Light::getLight(i);
        if(!list.contain(newLight)){
            if(light!= nullptr)*light=newLight;
            return i;
        }
    }
    return -1;
}

void ManagerRenderer::updateSize(){
    allL.iteratorFirst();
    while(allL.iteratorHasNext()){
        RenderObject * ro=allL.iteratorNext();
        ro->changeSize(ro->sizeU,ro->unitSize);
    }
}

void ManagerRenderer::updateHigh(){
    allL.iteratorFirst();
    while(allL.iteratorHasNext()){
        RenderObject * ro=allL.iteratorNext();
        ro->changeHigh(ro->highU,ro->unitHigh);
    }
}

void ManagerRenderer::updateSpeed(){
    allL.iteratorFirst();
    while(allL.iteratorHasNext()){
        RenderObject * ro=allL.iteratorNext();
        ro->changeSpeed(ro->speedU,ro->unitSpeed);
    }
}

void ManagerRenderer::updateSpeedA(){
    allL.iteratorFirst();
    while(allL.iteratorHasNext()){
        RenderObject * ro=allL.iteratorNext();
        ro->changeSpeedAxis(ro->speedAU,ro->unitSpeedA);
    }
}



