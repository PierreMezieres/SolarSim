//
// Created by argo on 12/06/17.
//

/******************************************************
 * ListRenderer, manage a list of RenderObject
 * List contains NodeRenderer
 *
 * NB:
 * Instead of using generic list, I prefered to
 * use precise list coded by myself to avoid
 * some useless calculation
 ******************************************************/

#ifndef SOLARSIM_LISTE_H
#define SOLARSIM_LISTE_H

#include "NodeRenderer.h"
#include "RenderObject.h"

class ListRenderer {
public:

    //Attribute
    NodeRenderer * startNode; //Start Node of the List
    NodeRenderer * iterator; //Iterator

    //Constructor
    explicit ListRenderer();

    void add(RenderObject * renderObject = nullptr);
    void remove(std::string id);
    void eraseAll();
    RenderObject * getROFromId(std::string id);

    /****** Iterator *****/
    void iteratorFirst();
    int iteratorHasNext();
    RenderObject * iteratorNext();


    void renderList();
    void calculatePosition(GLfloat time);



private:

};


#endif //SOLARSIM_LISTE_H
