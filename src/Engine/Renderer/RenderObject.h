//
// Created by argo on 13/06/17.
//

/******************************************************
 * RenderObject, "abstract" class for
 * Planet, Sun ...
 ******************************************************/

#ifndef SOLARSIM_RENDEROBJECT_H
#define SOLARSIM_RENDEROBJECT_H

using namespace std;
#include <string>
#include <GL/gl.h>
#include <glm/vec3.hpp>
#include <src/Engine/Texture.h>
#include <src/Core/Unit.h>

class RenderObject{
public:

    enum Child{Sun,Planet};

    struct Data{
        float val;
        string user;
        int unit;
        int unit2;
    };

    //Attribute
    std::string id;
    GLfloat position[4];
    float size; string sizeU; int unitSize;
    float high; string highU; int unitHigh;
    float speed; string speedU; int unitSpeed;
    float axis; string axisU; float valueAxis;
    float speedA; string speedAU; int unitSpeedA; int unitSpeedA2;
    Texture *texture;
    Child child;
    GLenum light; int num_light;

    //Constructor
    explicit RenderObject(Child child,std::string id,Texture * texture, float size=0, float hight=0, float speed=0., glm::vec3 pos = glm::vec3(0,0,0));

    void addPosition(GLfloat x, GLfloat y, GLfloat z);
    float conversionInput(float val,Unit::Unit_Term unit,float unitGlobal);
    float conversionInputTime(float val,Unit::Unit_Time unit,float unitGlobal);
    void changeSize(string size,int unitSize);
    void changeHigh(string high,int unitHigh);
    void changeSpeed(string speed,int unitSpeed);
    void changeAxisTilt(string axis);
    void changeSpeedAxis(string speedA,int unitSpeedA);
    string childToString();

    void debug();

    virtual void render();
    void calculatePosition(GLfloat time);

    //Texture GLuint
    ~RenderObject();



};

#endif //SOLARSIM_RENDEROBJECT_H
