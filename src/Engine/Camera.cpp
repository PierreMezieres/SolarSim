//
// Created by argo on 06/04/17.
//

#include <src/Debug/Log.h>
#include "Camera.h"
#include "../Core/Vector3.h"

// Constructor with vectors
Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch) :
        Front(glm::vec3(0.0f, 0.0f, 1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
{
    Position = position;
    WorldUp = up;
    Yaw = yaw;
    Pitch = pitch;
    Distance = DISTANCE;
    updateCameraVectors();
}
// Constructor with scalar values
Camera::Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) :
        Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
{
    Position = glm::vec3(posX, posY, posZ);
    WorldUp = glm::vec3(upX, upY, upZ);
    Yaw = yaw;
    Pitch = pitch;
    Distance = DISTANCE;
    updateCameraVectors();
}

// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
glm::mat4 Camera::GetViewMatrix()
{
    return glm::lookAt(Position, Position + Front, Up);
}

// Processes input received from any keyboard-like input system. Accepts input parameter in the form of Camera defined ENUM (to abstract it from windowing systems)
void Camera::ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime)
{
    GLfloat velocity = MovementSpeed * deltaTime;
    if (direction == FORWARD)
        Position += Front * velocity;
    if (direction == BACKWARD)
        Position -= Front * velocity;
    if (direction == LEFT)
        Position -= Right * velocity;
    if (direction == RIGHT)
        Position += Right * velocity;
}

// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
void Camera::ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch)
{
    xoffset *= MouseSensitivity;
    yoffset *= MouseSensitivity;

    Yaw   += xoffset;
    Pitch += yoffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (Pitch > 89.0f)
            Pitch = 89.0f;
        if (Pitch < -89.0f)
            Pitch = -89.0f;
    }

    // Update Front, Right and Up Vectors using the updated Eular angles
    updateCameraVectors();
}

void Camera::RecordParameters(){
    Position_r=Position;
    Front_r=Front;
    Up_r=Up;
    Right_r=Right;
    Distance_r=Distance;
}

void Camera::ReplaceParameters(){
    Position=Position_r;
    Front=Front_r;
    Up=Up_r;
    Right=Right_r;
    Distance=Distance_r;
}

void Camera::ProcessMouseMovementSphere(GLfloat xoffset, GLfloat yoffset)
{
    RecordParameters();

    GLfloat velocity = MovementSpeed;
    xoffset *= MouseSensitivity * Distance / 3.0f;
    yoffset *= MouseSensitivity * Distance / 3.0f;

    Position += Right * glm::vec3(-xoffset);
    Position += Up * glm::vec3(yoffset);



    float len = Vector3::length(Position);

    // Update Front, Right and Up Vectors using the updated Eular angles
    updateCameraVectorsSphere();

    Position += (len - Distance) * Front;

    float dot=Vector3::dotProduct(WorldUp,Position);
    if(Distance-glm::abs(dot)<0.1) ReplaceParameters();

}

// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
void Camera::ProcessMouseScroll(GLfloat yoffset)
{
    if (Zoom >= 1.0f && Zoom <= 45.0f)
        Zoom -= yoffset;
    if (Zoom <= 1.0f)
        Zoom = 1.0f;
    if (Zoom >= 45.0f)
        Zoom = 45.0f;
    GLfloat newDistance = Distance - yoffset * SCROLL_SENSITIVITY;
    if(newDistance > DistanceMin) {
        Position += Front * yoffset * SCROLL_SENSITIVITY;
        Distance = newDistance;
    }
}

void Camera::setDistance(float distance){ this->Distance=distance;}
void Camera::setDistanceMin(float distance){ this->DistanceMin=distance;}

void Camera::updateCameraVectors()
{
    // Calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    front.y = sin(glm::radians(Pitch));
    front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    Front = glm::normalize(front);
    // Also re-calculate the Right and Up vector
    Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    Up    = glm::normalize(glm::cross(Right, Front));
}

void Camera::updateCameraVectorsSphere()
{
    Front = glm::normalize(-Position);
    // Also re-calculate the Right and Up vector
    Right = glm::normalize(glm::cross(Front, WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    Up    = glm::normalize(glm::cross(Right, Front));
}

void Camera::look(){
    gluLookAt(Position.x,Position.y,Position.z,0,0,0,Up.x,Up.y,Up.z);
}