//
// Created by argo on 11/08/17.
//

#ifndef SOLARSIM_TEXTURE_H
#define SOLARSIM_TEXTURE_H


#include <GL/gl.h>
#include <string>
using namespace std;

class Texture{

public:
    string id;
    GLuint texture;


    explicit Texture(string id, GLuint texture);
    int textureEqual(Texture *texture);


};

#endif //SOLARSIM_TEXTURE_H
