//
// Created by argo on 06/04/17.
//

#ifndef PAINTGL_CAMERA_H
#define PAINTGL_CAMERA_H

/******************************************************
 * Class to manage the Camera
 ******************************************************/

// GL Includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// Default camera values
const GLfloat YAW        = -90.0f;
const GLfloat PITCH      =  0.0f;
const GLfloat SPEED      =  3.0f;
const GLfloat SENSITIVTY =  0.02f;
const GLfloat ZOOM       =  45.0f;
const GLfloat DISTANCE   =  3.0f;
const GLfloat DISTANCE_MIN = 0.f;
const GLfloat DISTANCE_MAX = 20.0f;
const GLfloat SCROLL_SENSITIVITY = 0.16f;

class Camera
{
public:


// Camera Attributes
    glm::vec3 Position, Position_r;
    glm::vec3 Front, Front_r;
    glm::vec3 Up, Up_r;
    glm::vec3 Right, Right_r;
    glm::vec3 WorldUp;
// Eular Angles
    GLfloat Yaw;
    GLfloat Pitch;
// Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;
    GLfloat Zoom;
    GLfloat DistanceMin=0.00001f,Distance, Distance_r;

    explicit Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH);
    explicit Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch);

    void ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime);

    void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true);
    void ProcessMouseMovementSphere(GLfloat xoffset, GLfloat yoffset);

    void ProcessMouseScroll(GLfloat yoffset);

    void setDistance(float distance);
    void setDistanceMin(float distance);

    glm::mat4 GetViewMatrix();
    void look();


private:

    void updateCameraVectors();
    void updateCameraVectorsSphere();
    void RecordParameters();
    void ReplaceParameters();

};

#endif //PAINTGL_CAMERA_H
